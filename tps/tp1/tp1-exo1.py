# import random

# variante
from random import randint

secret = randint(1, 10)

while True:
    answer = int(input("Devine mon chiffre secret: "))

    if answer == secret:
        break
    if answer > secret:
        print("Mon chiffre est plus petit")
    else:
        print("Mon chiffre est plus grand")

print("Bravo ! Le chiffre secret à deviner était bien %d" % secret)
