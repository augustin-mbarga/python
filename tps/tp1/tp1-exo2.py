from sys import argv

userInput = int(argv[1])

if userInput < 0 or userInput > 1000:
    print("Doit être comprise entre 0 et 1000 - Aurevoir")
    exit(1)  # 1 : return code

for n in range(11):
    
    # print("%d x %d = %d" % (userInput, n, userInput * n))
    # variante
    print(f"{userInput} x {n} = {userInput * n}")
