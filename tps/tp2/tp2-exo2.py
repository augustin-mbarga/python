acc = []
somme = 0
chaine = []

number = int(input("saisir un chiffre : "))
if number == 0:
    exit(1)

while number != 0:
    acc.append(number)
    chaine.append(str(number))
    somme += number
    number = int(input("saisir un chiffre : "))

print(somme)

for k in acc:
    print(k)

# préparation de ma chaine pour le récap d'après
chaine = ",".join(chaine)

# Affichage de => Somme des valeurs saisies: 20 (15,2,3)
print("=> Somme des valeurs saisies : %d (%s)" % (somme, chaine))
