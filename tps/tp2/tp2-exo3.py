list1 = [10, 20, 30, 40, 50]

# version 1 : 
# list1.reverse()

# boucle pour afficher les valeurs de list1 inversée
# for k in list1:
#     print(k)

# version 2 : 
for k in reversed(list1):
    print(k)
