from sys import argv

# dictionnaire de test
sample_dict = {'a': 100, 'b': 200, 'c': 300}


def isItInDict(value):
    # boucle sur uniquement les valeurs du dictionnaire sample_dict
    for v in sample_dict.values():
        if value == v:
            return True
    return False

# version 1 : 
# userInput = int(input("saisir la valeur à vérifier : "))
# if isItInDict(userInput):
#     print(f"{userInput} present in a dict")
# else:
#     print(f"{userInput} NOT present in a dict")
#     exit(1)


# version 2 : ma variante avec argv
if isItInDict(int(argv[1])):
    print(f"{argv[1]} present in a dict")
else:
    print(f"{argv[1]} NOT present in a dict")
    exit(1)
