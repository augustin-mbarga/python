'''
**********************************************************************
*  Auteur        : © Augustin MBARGA                                 *
*  Module Python : TP4                                               *
*  Date          : 02/01/2022                                        *
*  Révision      : 0                                                 *
**********************************************************************
'''
# import du module csv et os
import csv
import os

# la classe PageMaker
class PageMaker:
    # propriétés
    title = ""
    year = ""
    score = ""
    csv_rows = []

    # constructeur
    def __init__(self, file_path):
        with open(file_path, 'r') as csv_file:
            rows = csv.reader(csv_file, delimiter=',') 
            lineCount = 0
            for r in rows:
                if lineCount != 0: # pas la ligne d'entête
                    self.csv_rows.append({"year": r[0], "score": r[1], "title": r[2]})
                lineCount += 1
    
    # méthode generate_html
    def generate_html(self, out_dir):
        # création du repertoire si inexistant
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)

        i = 1  # valeur utilisée dans le nommage des fichiers HTML générés

        # ouverture du fichier HTML
        with open('template.html', 'r', encoding='Utf8') as f:
            # lecture du template html
            html_rows = f.readlines()

        for row in self.csv_rows:
            # boucle pour effectuer les changement avant d'écrire dans un nouveau fichier
            for r in html_rows:
                with open(out_dir + '/' + 'film_'+ str(i) + '.html', 'a', encoding="Utf8") as e:
                    if '<h1>' in r:
                        r = f"<h1>Film: {row['title'][2:-1]}</h1>\n"

                    elif '<title>' in r:
                        r = f"  <title>De Niro Movies - {row['title'][2:-1]}</title>\n"

                    elif '[Year]' in r:
                        r = f"  <p>Année de sortie: {row['year']}</p>\n"

                    elif '[Score]' in r:
                        r = f"  <p>Score obtenue: {row['score'][1:]}</p>\n"

                    e.write(r)
            i += 1

# Main
pm = PageMaker('deniro.csv')
pm.generate_html('./tmp')