'''
**********************************************************************
*  Auteur        : © Augustin MBARGA                                 *
*  Module Python : TP3 - Exercice 2                                  *
*  Date          : 31/12/2022                                        *
*  Révision      : 0                                                 *
**********************************************************************
'''
# import du module csv
import csv

# déclaration et assignation des variables
inputFile = 'deniro.csv'
outputFile = 'deniro-report.txt'
count = 0
selectedDict = {'Year': '', ' "Score"': '0', ' "Title"': ''}

try:
    # ouverture du fichier CSV
    with open(inputFile, mode='r') as file:

        # lecture du fichier CSV et boucle sur la list de dict générée par la fonction DictReader du module CSV
        for line in csv.DictReader(file):

            # copie du dict line si son score est supérieur au score de selectedDict (actuel champion)
            if int(line[' "Score"']) > int(selectedDict[' "Score"']):
                selectedDict = line.copy()

            # incrémentation de count si l'année de sortie du film est entre 2000 et 2010 ([2000;2010])
            if 2000 <= int(line['Year']) <= 2010:
                count += 1

        # résultats
        bestMovie = selectedDict[' "Title"'][2:-1]
        bestScore = selectedDict[' "Score"'][1:]

    # écriture dans le fichier (y compris création si fichier inexistant)
    f = open(outputFile, "w", encoding="Utf8")
    f.write("[+] Nom du film le mieux noté : %s (score -> %s)\n" %
            (bestMovie, bestScore))
    f.write("[+] Nombre de films entre 2000 et 2010 : %d\n" % count)

    # fermeture du fichier
    f.close()

except IOError:
    print("Problème lors la manipulation de fichier.")
    exit(27)

except FileNotFoundError:
    print("Fichier introuvable.")
    exit(37)

except csv.Error as error:
    raise ValueError(error)
    exit(47)
