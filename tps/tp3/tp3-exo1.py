'''
**********************************************************************
*  Auteur        : © Augustin MBARGA                                 *
*  Module Python : TP3 - Exercice 1                                  *
*  Date          : 30/12/2022                                        *
*  Révision      : 1                                                 *
**********************************************************************
'''
# import des 2 fonctions du module os nécessaires au programme
from os import mkdir, listdir, rename

# import des fonctions copy2, SameFileError du module shutil
from shutil import copy2, SameFileError

# regroupement des variables dans un dictionnaire
d = {
    "source": "flags",       # dossier source
    "dest": "flagsBis",      # dossier de destination
    "intruder": "missing",   # l'intrus à filtrer/ignorer
    "tr": 2,                 # slicing des 2 premières lettres
    "ext": ".png"            # extension des fichiers
}


def filesListFilter(s, i):
    # filtre pour ignorer le fichier missing.png
    return [x for x in listdir(s) if not x.startswith(i)]


def copyOneFile(s, f, d):
    # préparation du chemin de la source du fichier à copier
    filePath = s + "/" + f

    # copie le contenu de sourceFile dans destination
    copy2(filePath, d)


def renameOneFile(d, f, tr, ext):
    oldName = d + "/" + f
    newName = d + "/" + f[:tr].upper() + ext

    # on renomme le fichier
    rename(oldName, newName)


def kernel():
    # création du dossier qui va recevoir une copie des fichiers
    mkdir(d["dest"])

    # Appel de la fonction filtre pour ignorer le fichier intrus
    files = filesListFilter(d["source"], d["intruder"])

    # boucle sur la list filtrée pour copier et renommer les fichiers
    for file in files:
        # Appel de la fonction qui copie un fichier
        copyOneFile(d["source"], file, d["dest"])

        # Appel de la fonction qui renomme un fichier
        renameOneFile(d["dest"], file, d["tr"], d["ext"])

        # Affichage du message de succès de l'opération sur un fichier
        print("[+] File %s copied and renamed successfully." % file)


try:
    kernel()
except SameFileError:
    print("Problème de fichiers identiques.")
    exit(10)

except FileExistsError:
    print("Probème d'existence de fichier.")
    exit(20)

except PermissionError:
    print("Problème de permission.")
    exit(30)

except FileNotFoundError:
    print("Problème de localisation de fichier.")
    exit(40)
