'''
**********************************************************************
*  Auteur        : © Augustin MBARGA                                 *
*  Module Python : TP3 - Exercice 1                                  *
*  Date          : 30/12/2022                                        *
*  Révision      : 0                                                 *
**********************************************************************
'''
# import des 2 fonctions du module os nécessaires au programme
from os import mkdir, listdir, rename

# import des fonctions copy2, SameFileError du module shutil
from shutil import copy2, SameFileError

# déclaration et assignation de variables
source = "flags"        # dossier source
dest = "flagsBis"       # dossier de destination
intruder = "missing"    # l'intrus à filtrer/ignorer
trunc = 2               # slicing des 2 premières lettres

try:
    # création du dossier qui va recevoir une copie des fichiers
    mkdir(dest)

    # filtre pour ignorer le fichier missing.png
    files = [x for x in listdir(source) if not x.startswith(intruder)]

    # boucle sur la list filtrée pour copier et renommer les fichiers
    for file in files:

        # préparation du chemin de la source du fichier à copier
        filePath = source + "/" + file

        # copie le contenu de sourceFile dans destination
        copy2(filePath, dest)

        # on renomme le fichier
        rename(dest + "/" + file, dest + "/" + file[:trunc].upper() + ".png")
        print("[+] File %s copied and renamed successfully." % file)

except SameFileError:
    print("Problème de fichiers identiques.")
    exit(10)

except FileExistsError:
    print("Probème d'existence de fichier.")
    exit(20)

except PermissionError:
    print("Problème de permission.")
    exit(30)

except FileNotFoundError:
    print("Problème de localisation de fichier.")
    exit(40)
