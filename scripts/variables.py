# variables primitives en Python
temperature = 2
pi = 3.14
isEarthRound = True
training = "Initiation au langage Python"

# Affichage du type des variables
print(type(temperature))    # int
print(type(pi))             # float
print(type(isEarthRound))   # bool
print(type(training))       # str

# print(2 + 2)
# print(temperature + temperature)
# training = "Perfectionnement au langage Python"
# print(training)

# Opérations sur les variables
# addition entre entiers
print(temperature + 10)
# concaténation entre deux chaînes
print(training + "10")
# addition entre un float et un int
print(pi + 10)
# conversion implicite, True => 1
print(isEarthRound + 10)
print("Le double de pi est: " + str(pi * 2))

# stockage en variable du retour d'une expression arithmétique
doublePi = pi * 2
print(type(doublePi))  # float
doublePiStr = str(doublePi)
print(type(doublePiStr))  # str

