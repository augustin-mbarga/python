# liste de 3 entiers
numbers = [23, 140, 0]
print(type(numbers))  # list

for n in numbers:
    print(n)

first = numbers[0]
print(first)
computedIndex = 1 + 1
print(numbers[computedIndex])

count = 0
while count < 3:
    print(numbers[count])
    count += 1

title = "Les trois Mousquetaires"
print(title[4])
for c in title:
    print(c)

acc = 0  # variable accumulateur
search = "e"
for c in title:
    if c == search:
        acc += 1

print(f"Le caractère {search} a été trouvé {acc} fois dans le titre {title}")
