from sys import argv

from funct import add_numbers, delta_numbers
# from funct import *
# import funct

print(f" Somme : {int(argv[1])} + {int(argv[2])} = {add_numbers(int(argv[1]), int(argv[2]))}")
print(f" Ecart : {delta_numbers(int(argv[1]), int(argv[2]))}")
# print(f" {int(argv[1])} + {int(argv[2])} = {funct.add_numbers(int(argv[1]), int(argv[2]))}")
