class Student:
    # propriétés (variable encapsulée dans une classe)
    first_name = ""
    last_name = ""

    # propriété privée (private)
    __age = 0

    # méthodes (fonction encapsulée dans une classe)
    def test(self):
        print("test")

    # accesseur (getter)
    def get_age(self):
        return self.__age
    
    # méthode superflux car la propriété first_name est public
    def get_first_name(self):
        return self.first_name

    def set_age(self, age):
        self.__age = age

# instanciation
s1 = Student()
s2 = Student()
# print(type(s1))

s1.first_name = "Toto" # accès en écriture
print(s1.first_name)

# invocation d'une méthode à partir d'un objet
s1.test()
s2.test()

# print(s1.age)
print(s1.get_age())

s1.set_age(18)
print(s1.get_age())

s2.set_age(99)
print(s2.get_age())

print("\n-----------------------------------------\n")

class Vehicule:
    __num_wheels = 0
    __max_speed = 100

    # méthodes
    # constructeurs
    def __init__(self, num_wheels, max_speed=100):
        # print("Hello from constructor method !")
        self.__num_wheels = num_wheels
        self.__max_speed = max_speed

    def get_num_wheels(self):
        return self.__num_wheels
    
    def get_max_speed(self):
        return self.__max_speed

v1 = Vehicule(2, 90)
v2 = Vehicule(4, 180)

print(v1.get_num_wheels())
print("Vitesse max du véhicule v1: ", v1.get_max_speed())
print("Vitesse max du véhicule v2: ", v2.get_max_speed())
