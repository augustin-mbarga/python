postCodes = [
    67200, 75012, 68520, 15000, 75020,
    67200, 75012, 68520, 15000, 75020,
    67275, 75012, 68520, 17750, 75020,
    75007, 75012, 68520, 75000, 75020
]

# Combien de codes postaux parisiens ?
# 1. variable d'accumulation
acc = 0
# 2. parcourir la liste
# Approche 1
for postCode in postCodes:
    #     # 3. recherche tout ce qui commence par 75
    #     if postCode >= 75000 and postCode <= 75999:
    #         # 4. incrémente accu quand parisien trouvé
    #         acc += 1

    # Approche 2 ("commence par")
    postCodeStr = str(postCode)
    # slicing. Slice des 2 premiers caractères (indices 0 et 1. 2 exclu) => on peut aussi écrire [:2]
    dpt = postCodeStr[:2]
    if dpt == "75":
        acc += 1

print(f"Nombre de codes postaux parisiens: {acc}")

# Autre exemple de "slicing"
# Affichage des 3 derniers codes postaux de la liste initiale
print(postCodes[-3:])  # de la position -3 jusqu'à la fin

for p in postCodes[-3:]:
    print(p)
