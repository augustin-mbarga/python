'''
Objectif: afficher 5 fois "Bonjour"

Solution 1
print("Bonjour")
print("Bonjour")
print("Bonjour")
print("Bonjour")
print("Bonjour")
'''

# Solution 2, boucle while
count = 1
while count <= 5:
    print("Bonjour")
    count += 1  # incrémentation de 1 (des steps de 1)

count2 = 5
while count2 > 0:
    print("Bye", count2)
    count2 -= 1  # décrémentation de 1 (des steps de -1)

# Solution 3, boucle for..in
l = range(5)
print(type(l))
for n in range(100):
    print("Coucou n°", n)
    if n == 4:
        break
